import os

import numpy as np
from PIL import Image
from dotenv import load_dotenv
from flask import Flask, request  # pip install flask
from keras_preprocessing import image
from tensorflow.python.keras.models import load_model
from werkzeug.utils import secure_filename
import werkzeug
from flask_cors import CORS

load_dotenv()

UPLOAD_FOLDER = os.getenv('UPLOAD_DIR')
MODELS_DIR = os.getenv('MODELS_DIR')
ALLOWED_EXTENSIONS = {'png', 'jpeg', 'PNG', 'jpg'}
ALLOWED_EXTENSIONS_MODEL = {'keras'}
MODEL_TYPES = {'linear', 'mlp', 'cnn', 'rnn'}
MODELS = {'linear': 'linear.keras', 'mlp': 'mlp.keras', 'cnn': 'cnn.keras', 'rnn': 'rnn.keras'}
CATEGORIES = {"action", "ecchi", "fantasy", "slice of life"}
ACCEPTED_VALUES = [0.65, 0.65, 0.65, 0.65]

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MODELS_DIR'] = MODELS_DIR


cors = CORS(app, resources={r"/*": {"origins": "*"}})


def allowed_file(filename, allowed):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in allowed


def check_picture_with_model(filepath, model_path):
    return check_picture(filepath, model_path)


def check_picture_with_type(filepath, model):
    m = MODELS[model]
    if m is None:
        handle_bad_request()
    return check_picture(filepath, os.path.join(app.config['MODELS_DIR'], MODELS[model]))


def save_temp_file(file, allowed):
    if file.filename == '':
        handle_bad_request()
    if file and allowed_file(file.filename, allowed):
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)
        return filepath
    handle_bad_request()


def check_picture(filepath, model_path):
    target_resolution = (225, 315)

    model = load_model(model_path)

    img = Image.open(filepath).resize(target_resolution)
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    images = np.vstack([x])

    return format_result(model.predict(images)[0])


def format_result(preds):
    final = {'results': []}

    for index, cat in enumerate(CATEGORIES):
        value = preds[index].item()
        percentage = str(value * 100) + ' %'
        accepted_value = ACCEPTED_VALUES[index]
        is_accepted = value >= accepted_value
        final['results'].append({'name': cat, 'score': percentage, 'accepted': is_accepted})

    return final


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request():
    return 'bad request!', 400


@app.route('/', methods=['POST'])
def analyse_picture():
    if 'file' not in request.files:
        handle_bad_request()
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    filepath = save_temp_file(file, ALLOWED_EXTENSIONS)

    if request.form.get('type') in MODEL_TYPES:
        return check_picture_with_type(filepath, request.form['type'])
    elif 'model' in request.files:
        model_file = request.files['model']
        model_path = save_temp_file(model_file, ALLOWED_EXTENSIONS_MODEL)
        return check_picture_with_model(filepath, model_path)
    return handle_bad_request()


if __name__ == "__main__":
    app.run(host='0.0.0.0')
