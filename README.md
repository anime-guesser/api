# Anime Guesser API

## Prérequis

Variables d'environnements
   - ``UPLOAD_DIR`` : chemin local du dossier où stocker les fichiers envoyés par l'api
   - ``MODELS_DIR`` : chemin local du dossier où l'api ira chercher les .keras à utiliser
   
Les modèles pré-entrainés situés dans ```MODELS_DIR``` doivent avoir le nommage suivant :
 - Linear model : ``linear.keras``
 - Neural Network Model : ``mlp.keras``
 - Convolutional Neural Network : ``cnn.keras``
 - Residual Neural Network : ``rnn.keras``
 
 Ne pas oublier d'installer les dépendances écrites dans ``requirements.txt``